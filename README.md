# medinfo2019-presentation

framagit is not yet compatible with papeera. I created a gitlab folder.

how to sync from git (gitlab https://gitlab.com/parisni/medinfo2019-presentation ):

1. add the ssh-key of your papeera account into gitlab 
    1. from papeera: setting > ssh-key > copy it 
    2. from gitlab : setting > ssh > paste it
2. look on the right: the "version control" button ? click on it and then on git.

It is then possible to clone the repository, work locally, and pull it from papeerah


##